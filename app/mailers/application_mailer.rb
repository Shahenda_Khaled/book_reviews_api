class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@trianglz.com'
    layout 'mailer'
end
