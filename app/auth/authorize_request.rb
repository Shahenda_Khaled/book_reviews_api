class AuthorizeRequest

    attr_reader :headers

    def initialize(headers = {})
        @headers = headers
    end

    def call
        # debugger
        user
    end

    private
    
    def user
        # request_header_auth
        # d_token = decode_token
        @user ||= User.find(decoded_token[:user_id]) if decoded_token

    rescue ActiveRecord::RecordNotFound => e
        raise(ExceptionHandler::InvalidToken, "#{Message.invalid_token} #{e.message}")
    end

    def decoded_token
        # debugger
        @decoded_token ||= JsonWebToken.decode(request_header_auth)
    end
    
    def request_header_auth
        # debugger
        # authotization = 
        if @headers['Authorization'].present?
            @headers['Authorization'].split(" ").last
        else
            raise(ExceptionHandler::MissingToken, Message.missing_token)
        end
    end

end