class Message
  def self.not_found(record = "record")
    "Sorry, #{record} not found."
  end

  def self.invalid_credentials
    "Invalid credentials"
  end

  def self.invalid_token
    "Invalid token"
  end

  def self.missing_token
    "Missing token"
  end

  def self.unauthorized
    "Unauthorized request"
  end

  def self.account_created
    "Account created successfully"
  end

  def self.account_not_created
    "Account could not be created"
  end

  def self.expired_token
    "Sorry, your token has expired. Please login to continue."
  end

  def self.account_activated_successfully
    "Account activated successfully. Please log in"
  end

  def self.invalid_activation_link
    "Invalid activation link"
  end

  def self.activate_account_via_email
    "Email has been sent to activate account"
  end

  def self.login_successful
    "Login successful"
  end

  def self.book_created_successfully
    "Book created successfully"
  end

  def self.book_review_created_successfully
    "Book review created successfully"
  end

  def self.book_already_reviewed
    "Book already reviewed"
  end
end
