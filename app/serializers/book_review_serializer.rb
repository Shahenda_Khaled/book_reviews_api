class BookReviewSerializer < ActiveModel::Serializer
  attributes :id, :review, :rating
  
  belongs_to :user
  belongs_to :book
end
