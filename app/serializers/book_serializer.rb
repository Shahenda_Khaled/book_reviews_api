class BookSerializer < ActiveModel::Serializer
  attributes :id, :title, :avg_rating
end
