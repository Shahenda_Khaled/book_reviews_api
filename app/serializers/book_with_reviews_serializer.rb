class BookWithReviewsSerializer < ActiveModel::Serializer
  attributes :id, :title, :avg_rating

  has_many :book_reviews
end
