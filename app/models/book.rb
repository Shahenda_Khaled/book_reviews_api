class Book < ApplicationRecord
    belongs_to :user
    has_many :book_reviews, dependent: :destroy
    has_many :reviewers, through: :book_reviews, source: :user

    mount_uploader :image, ImageUploader
    validates_presence_of :title
end
