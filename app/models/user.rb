class User < ApplicationRecord
    has_many :book_reviews, dependent: :destroy
    # has_many :book_reviews, class_name: "BookReview", foreign_key: "book_id", dependent: :destroy
    has_many :read_books, through: :book_reviews, source: :book
    has_many :books

    # has_many :apps_genres, :primary_key => :application_id, :foreign_key => :application_id
    # has_many :read_books, :through => :apps_genres

    attr_accessor :activation_token

    has_secure_password

    validates_presence_of :name, :email, :password_digest
    validates_uniqueness_of :email

    before_create :create_activation_digest

    def correct_activation?(token)
        digest = self.send("activation_digest")
        return false if digest.nil?
        BCrypt::Password.new(digest).is_password?(token)
    end

    private 

    def new_token
        SecureRandom.urlsafe_base64
    end

    def digest(string)
        cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
        BCrypt::Engine.cost
        BCrypt::Password.create(string, cost: cost)
    end

    def create_activation_digest
        self.activation_token = new_token
        self.activation_digest = digest(activation_token)
    end
end
