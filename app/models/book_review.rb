class BookReview < ApplicationRecord
  belongs_to :user
  belongs_to :book

  validates_presence_of :review, :rating
  validates :rating, length: { miniumum: 1, maximum: 5 }
end
