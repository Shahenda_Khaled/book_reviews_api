class BooksController < ApplicationController
  def create
    @book_attrs = book_params
    if !@book_attrs['image'].nil?
      @book_attrs['image'] = Pathname.new(Rails.root.join("app/assets/images/#{book_params[:image]}")).open
    end
    if book_params[:avg_rating].nil?
      @book_attrs['avg_rating'] = 0
    end
    book = @current_user.books.create!(@book_attrs)
    response = { result: book, message: Message.book_created_successfully }
    render json: response, status: :created, serializer: BookSerializer
    # json_response(response, :created)
  end

  def index
    books = Book.all
    json_response_with_serializer(books, :ok, BookWithReviewsSerializer)
  end

  private

  def book_params
    params.permit(:title, :avg_rating, :image)
  end
end
