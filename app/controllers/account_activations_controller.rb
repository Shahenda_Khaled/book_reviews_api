class AccountActivationsController < ApplicationController

    skip_before_action :authorize_request, only: :edit

    def edit 
        user = User.find_by(email: params[:email])
        if user && !user.active? && user.correct_activation?(params[:id])
            user.update_attribute(:active, true)
            # user_token = AuthenticateUser.new(user.email, user.password).call
            json_response({ message: Message.account_activated_successfully })
        else
            json_response({ message: Message.invalid_activation_link})
        end
    end

end
