class ApplicationController < ActionController::API
    include JsonResponse
    include ExceptionHandler

    before_action :authorize_request
    attr_reader :current_user


    def authorize_request
        # debugger
       @current_user = AuthorizeRequest.new(request.headers).call
    end
end
