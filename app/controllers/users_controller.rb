class UsersController < ApplicationController

    skip_before_action :authorize_request, only: [:create, :login]

    def create
        user = User.create!(user_params)
        UserMailer.account_activation(user).deliver_now
        response = { messages: [Message.account_created, Message.activate_account_via_email], activated: false}
        json_response(response, :created)
    end

    def login
        auth_token = AuthenticateUser.new(user_params[:email], user_params[:password]).call
        json_response({auth_token: auth_token, message: Message.login_successful})

    end

    private 
    def user_params
        params.permit(:name, :email, :password, :password_confirmation)
    end
end
