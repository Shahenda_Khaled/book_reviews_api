class BookReviewsController < ApplicationController
  def create
    book = Book.find(params[:book_id])
    read_books = @current_user.read_books
    if read_books && read_books.include?(book)
      response = { message: Message.book_already_reviewed }
      json_response(response)
    else
        review = @current_user.book_reviews.create!(review_params)
        response = { result: review, message: Message.book_review_created_successfully }
        json_response(response, :created)
        if book.reviewers && book.reviewers.count > 1
          rating = (book.avg_rating + review_params[:rating]) / (book.reviewers.count)
        else
          rating = review_params[:rating]
        end
        book.update_attribute(:avg_rating, rating)
    end
  end

  def index
    book = Book.find(params[:book_id])
    json_response(book.book_reviews)
  end

  private

  def review_params
    params.permit(:review, :rating, :book_id)
  end
end
