module JsonResponse
  def json_response(object, status = :ok)
    render json: object, status: status
  end

  def json_response_with_serializer(object, status = :ok, serializer)
    render json: object, status: status, each_serializer: serializer
  end
end
