Rails.application.routes.draw do

  post 'signup', to: 'users#create'
  post 'login', to: 'users#login'
  resources :account_activations, only: [:edit]
  resources :books, only: [:create, :index] do
    resources :book_reviews, only: [:create, :index]
  end
  
end
