class CreateBooks < ActiveRecord::Migration[6.0]
  def change
    create_table :books do |t|
      t.string :title
      t.decimal :avg_rating
      t.string :image

      t.timestamps
    end
    add_index :books, :title
  end
end
