require 'rails_helper'

RSpec.describe BookReview, type: :model do

  it { should belong_to(:user) }
  it { should belong_to(:book) }
  it { should validate_presence_of(:review) }
  it { should validate_presence_of(:rating) }

end
